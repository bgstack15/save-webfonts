# Readme for save-webfonts
This `save-webfonts` script and accompanying library are designed to facilitate caching webfonts manually for approved sites. Many sites out there use fonts for interesting glyphs, so disabling webfonts is not a choice made for convenience but security.
But for the sites you trust (or are willing to compromise for), you can run save-webfonts with those URLs, and save down the webfonts to your local user font cache.

## Features

* Read css files from har file (web interaction replay log generated by a web browser)
* Convert woff, woff2, and eot down to ttf

## Upstream
[This repo](https://gitlab.com/bgstack15/save-webfonts) is the original source.

## Alternatives
Enable webfonts in your browser. Check setting `gfx.downloadable_fonts.enabled`. The author is not aware of any other scripts, processes, or extensions that "allow" web fonts for arbitrary sites.

This is original research.

## How to use
Read the output of `./save-webfonts --help`, which is also provided here.

    usage: save-webfonts [-h] [-d [{0,1,2,3,4,5,6,7,8,9,10}]] [-v] [-n | -a] [-o OUTDIR] [-w | -N] url [url ...]

    Download webfonts for a URL so browsers do not need to fetch them

    positional arguments:
      url                   URLs whose webfonts should be downloaded

    optional arguments:
      -h, --help            show this help message and exit
      -d [{0,1,2,3,4,5,6,7,8,9,10}], --debug [{0,1,2,3,4,5,6,7,8,9,10}]
                            Set debug level
      -v, --version         show program's version number and exit
      -n, --dryrun          Make no changes (default)
      -a, --apply           Actually make changes
      -o OUTDIR, --outdir OUTDIR
                            Location of font directory. Default is ~/.local/share/fonts
      -w, --wait            Wait at end for Return key
      -N, --no-wait, --nowait
                            Do not wait. Default.

## Dependencies
These are available as system packages in Fedora and Devuan.
Hard dependencies

* python3-beautifulsoup4 | python3-bs4
* python3-tinycss2

Recommends

* python3-fonttools
* libeot-tools | eot2ttf

## Bugs

* Frontend does not support har files. You have to load the python library and call the function.

## Improve

* Add harfile support to frontend script

## References
None

## Differences from upstream
None
